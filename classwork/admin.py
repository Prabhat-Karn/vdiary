from django.contrib import admin
from classwork.models import AssignClasswork
# Register your models here.
class AssignClassworkAdmin(admin.ModelAdmin):
    list_display = ('id','teacher','classes','lecture','subject','title','description','date_created')
    search_fields = ('id','title')

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

admin.site.register(AssignClasswork, AssignClassworkAdmin)