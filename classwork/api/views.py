from rest_framework.views import APIView
from rest_framework.response import Response
from Vdiary.permissions import IsParent, IsTeacher, IsStudent
from rest_framework.permissions import IsAuthenticated

from classwork.models import AssignClasswork
from classwork.api.serializers import AssignClassworkSerializer
from profiles.models import Student
from django.views import View
from django.shortcuts import render, redirect

from django.db.models import Q

from attendance.models import TeacherAttendance


class AssignClassworkView(APIView):
    serializer_class = AssignClassworkSerializer
    
    def get_permissions(self):
        if self.request.method == 'GET':
            permission_classes = (IsAuthenticated,IsParent | IsStudent,)
        else:
            permission_classes = (IsAuthenticated,IsTeacher,)
        return [permission() for permission in permission_classes]

    def get_object(self, request):
        try:
            return AssignClasswork.objects.filter(Q(classes__student__parent__father_contact_number = request.user) |
            Q(classes__student__registration_number = request.user)).distinct()
        except AssignClasswork.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def get_classwork(self, pk):
        return AssignClasswork.objects.get(pk=pk)

    def get(self, request, pk=None, format = None):
        classwork = self.get_object(request)
        serializer = AssignClassworkSerializer(classwork, many = True)
        return Response(serializer.data)

    def post(self, request, pk=None, format = None):
        attendance_data =TeacherAttendance.objects.filter(teacher__contact_number = request.user)
        teacher = list(attendance_data)[-1].teacher_id
        classes = list(attendance_data)[-1].classes_id
        lecture = list(attendance_data)[-1].lecture_id
        subject = list(attendance_data)[-1].subject_id

        serializer = AssignClassworkSerializer(data = request.data)
        data = {}
        if serializer.is_valid():
            classwork = serializer.save(teacher_id = teacher, classes_id = classes, lecture_id = lecture, subject_id = subject)
            data['response'] = 'Classwork Assigned'

        else:
            data = serializer.errors

        return Response(data)

    def patch(self, request, pk, format = None):
        classwork = self.get_classwork(pk)
        serializer = AssignClassworkSerializer(classwork,data = request.data, partial = True)
        data = {}
        if serializer.is_valid():
            classwork = serializer.save()
            data['response'] = 'Classwork Updated!'

        else:
            data = serializer.errors

        return Response(data)

