from django.urls import path
from classwork.api import views

app_name = 'classwork'


urlpatterns = [
    path('classwork/', views.AssignClassworkView.as_view(), name = 'classwork'),
    path('classwork/<int:pk>/', views.AssignClassworkView.as_view(), name = 'classwork'),
]