from rest_framework import serializers
from classwork.models import AssignClasswork

class AssignClassworkSerializer(serializers.ModelSerializer):
    class Meta:
        model = AssignClasswork
        fields = ['title','description','classwork_file','date_created']