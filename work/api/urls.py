from django.urls import path
from work.api import views

app_name = 'work'


urlpatterns = [
    path('work/', views.AssignWorkView.as_view(), name = 'work'),
    path('class_activity/', views.ClassActivity.as_view(), name = 'class activity'),
]