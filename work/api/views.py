from rest_framework.views import APIView
from rest_framework.response import Response
from Vdiary.permissions import IsParent, IsTeacher, IsStudent, IsPrincipal
from rest_framework.permissions import IsAuthenticated
from django.views import View
from django.shortcuts import render, redirect

from work.api.serializers import WorkSerializer, GetWorkSerializer
from attendance.models import TeacherAttendance
from work.models import AssignWork

from rest_framework.authentication import SessionAuthentication, TokenAuthentication

from rest_framework.status import(
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from django.db.models import Q

class AssignWorkView(APIView):
    def get_permissions(self):
        if self.request.method == 'GET':
            permission_classes = (IsAuthenticated,IsParent | IsStudent | IsTeacher,)
        else:
            permission_classes = (IsAuthenticated,IsTeacher)
        return [permission() for permission in permission_classes]
    # def get_object(self, request):
    #     try:
    #         return AssignWork.objects.filter(Q(classes__student__parent__father_contact_number = request.user) |
    #         Q(classes__student__registration_number = request.user) |
    #         Q(teacher__contact_number = request.user)).distinct()
    #     except AssignHomework.DoesNotExist:
    #         return Response(status=status.HTTP_404_NOT_FOUND)
    
    def get_attendance(self, request):
        attendance_data =TeacherAttendance.objects.filter(teacher__contact_number = request.user)
        return attendance_data

    def get_teacher(self, request):
        teacher = list(self.get_attendance(request))[-1].teacher_id
        return teacher

    def get_classes(self, request):
        classes = list(self.get_attendance(request))[-1].classes_id
        return classes

    def get_lecture(self, request):
        lecture = list(self.get_attendance(request))[-1].lecture_id
        return lecture
    
    def get_subject(self, request):
        subject = list(self.get_attendance(request))[-1].subject_id
        return subject

    # def get(self, request, format = None): #api for class activity
    #     work = self.get_object(request)
    #     serializer = GetWorkSerializer(work, many = True)
    #     return Response(serializer.data)

    def post(self, request, format = None):
        serializer = WorkSerializer(data = request.data, context = {'request':request})
        data = {}
        if serializer.is_valid():
            work = serializer.save(teacher_id = self.get_teacher(request), 
                                    classes_id = self.get_classes(request),
                                    lecture_id = self.get_lecture(request),
                                    subject_id = self.get_subject(request)
                                )

            data['response'] = 'Homework Assigned'

        else:
            data = serializer.errors

        return Response(data)

class ClassActivity(APIView): #API for classActivity
    permission_classes = (IsAuthenticated,IsParent | IsStudent | IsTeacher,)
    def get_work_for_parent(self, request):
        try:
            return AssignWork.objects.filter(Q(classes__student__registration_number__registration_number = request.data['registration_number'])&
                                            Q(date_created__range = [request.data['from_date'],request.data['to_date']]))
        except AssignWork.DoesNotExist:
            return Response(status = HTTP_404_NOT_FOUND)

    def get_work(self,request):
        try:
            return AssignWork.objects.filter(
            Q(classes__student__registration_number = request.user) |
            Q(teacher__contact_number = request.user)).distinct()
        except AssignWork.DoesNotExist:
            return Response(status=HTTP_404_NOT_FOUND)

    def post(self, request, format = None):
        required_role = request.user.roles.all()
        if required_role.filter(Q(role = 'Parent')).exists():
            work = self.get_work_for_parent(request)
        else:
            work = self.get_work(request)
        serializer = GetWorkSerializer(work, many = True)
        return Response(serializer.data, status = HTTP_200_OK)