from rest_framework import serializers
from work.models import AssignWork

class WorkSerializer(serializers.ModelSerializer):
    class Meta:
        model = AssignWork
        fields = ['classwork_title','homework_title','homework_description','homework_file','date_created']

class GetWorkSerializer(serializers.ModelSerializer):
    lecture = serializers.CharField(source = 'lecture.lecture_no', read_only = True)
    subject = serializers.CharField(source = 'subject.subject_name', read_only = True)
    classes = serializers.CharField(source = 'classes.classes', read_only = True)
    section = serializers.CharField(source = 'classes.section', read_only = True)
    class Meta:
        model = AssignWork
        fields = ['lecture','subject','classes','section','classwork_title','homework_title','homework_description','homework_file','date_created']