from django.contrib import admin
from work.models import AssignWork
# Register your models here.
class AssignWorkAdmin(admin.ModelAdmin):
    list_display = ('id','teacher','classes','lecture','subject','classwork_title',
    'homework_title','homework_description','date_created')
    search_fields = ('id',)

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

admin.site.register(AssignWork, AssignWorkAdmin)