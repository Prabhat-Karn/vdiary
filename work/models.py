from django.db import models

# Create your models here.
from profiles.models import Teacher
from classes.models import ClassesSection
from lecture.models import Lecture
from subject.models import Subject
# Create your models here.
class AssignWork(models.Model):
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    classes = models.ForeignKey(ClassesSection, on_delete=models.CASCADE)
    # section = models.ForeignKey(Section, on_delete=models.CASCADE)
    lecture = models.ForeignKey(Lecture, on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    classwork_title = models.CharField(max_length=50)
    homework_title = models.CharField(max_length=50)
    homework_description = models.TextField(blank = True)
    homework_file = models.FileField(upload_to='uploads/', blank = True)
    date_created = models.DateField(verbose_name = "date created", auto_now_add=True)
    time_created = models.TimeField(verbose_name = "time created", auto_now_add=True)

    # def __str__(self):
    #     return self.title