from django.contrib import admin
from lecture.models import Lecture
from import_export.admin import ImportExportModelAdmin
from import_export import fields, resources
from import_export.widgets import ForeignKeyWidget
from import_export.widgets import TimeWidget
from datetime import datetime
# Register your models here.
class LectureResource(resources.ModelResource):
    def before_import_row(self, row, **kwargs):
        row['start_time']=row['start_time'].strftime("%H:%M:%S")
        row['end_time'] = row['end_time'].strftime("%H:%M:%S")
    class Meta:
        model = Lecture

class LectureAdmin(ImportExportModelAdmin,admin.ModelAdmin):
    resource_class = LectureResource
    list_display = ('lecture_no', 'week_days','teacher','classes','subject','start_time','end_time')
    
    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

admin.site.register(Lecture,LectureAdmin)