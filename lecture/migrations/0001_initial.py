# Generated by Django 3.1.2 on 2021-04-12 16:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('profiles', '0001_initial'),
        ('subject', '0001_initial'),
        ('classes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Lecture',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lecture_no', models.PositiveSmallIntegerField()),
                ('week_days', models.CharField(max_length=9)),
                ('start_time', models.TimeField()),
                ('end_time', models.TimeField()),
                ('date', models.DateField()),
                ('classes', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='classes.classessection')),
                ('subject', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='subject.subject')),
                ('teacher', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='profiles.teacher')),
            ],
            options={
                'unique_together': {('lecture_no', 'classes', 'week_days')},
            },
        ),
    ]
