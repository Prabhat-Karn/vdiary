from django.db import models
from subject.models import Subject
from classes.models import ClassesSection
from profiles.models import Teacher
# Create your models here.
class Lecture(models.Model):
    lecture_no = models.PositiveSmallIntegerField()
    week_days = models.CharField(max_length=9)
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    classes = models.ForeignKey(ClassesSection, on_delete=models.CASCADE)
    # section = models.ForeignKey(ClassesSection, on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    start_time = models.TimeField(auto_now=False, auto_now_add=False)
    end_time = models.TimeField(auto_now=False, auto_now_add=False)
    date = models.DateField(auto_now=False, auto_now_add=False)

    class Meta:
        unique_together = ('lecture_no','classes','week_days'),
    
    def __str__(self):
        return str(self.lecture_no)