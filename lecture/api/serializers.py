from rest_framework import serializers
from lecture.models import Lecture

class LectureSerializer(serializers.ModelSerializer):
    subject_name = serializers.ReadOnlyField(source = 'subject.subject_name')
    classes = serializers.CharField(source = 'classes.classes', read_only = True)
    section = serializers.CharField(source = 'classes.section', read_only = True)
    class Meta:
        model = Lecture
        fields = ['lecture_no','week_days','teacher','classes','section','subject_name','start_time','end_time','date']