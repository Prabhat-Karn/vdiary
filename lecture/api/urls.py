from django.urls import path
from lecture.api import views

app_name = 'lecture'


urlpatterns = [
    path('lecture/',views.LectureView.as_view()),
    
]