from rest_framework.views import APIView
from rest_framework.response import Response
from Vdiary.permissions import IsParent, IsTeacher, IsStudent
from rest_framework.permissions import IsAuthenticated

from lecture.api.serializers import LectureSerializer 
from lecture.models import Lecture

from rest_framework.authentication import SessionAuthentication, TokenAuthentication

class LectureView(APIView):
    serializer_class = LectureSerializer
    authentication_classes = (SessionAuthentication,TokenAuthentication)
    permission_classes = (IsAuthenticated,IsTeacher,)

    def get_object (self, request):
        try:
            return Lecture.objects.filter(teacher__contact_number=request.user)
        except Lecture.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, format = None):
        lecture = self.get_object(request)
        serializer = LectureSerializer(lecture, many = True)
        return Response(serializer.data)