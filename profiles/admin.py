from django.contrib import admin
from profiles.models import Parent, Teacher, Student
from account.models import Account
from import_export.admin import ImportExportModelAdmin
from import_export import fields, resources
from import_export.widgets import ForeignKeyWidget
from classes.models import ClassesSection

# Register your models here.
class ParentResource(resources.ModelResource):
    father_email = fields.Field(
        column_name = 'father_email',
        attribute = 'father_email'
    )

    father_name = fields.Field(
        column_name = 'father_name',
        attribute = 'father_name'
    )
    
    father_contact_number = fields.Field(
        column_name = 'father_contact_number',
        attribute = 'father_contact_number',
        widget = ForeignKeyWidget(Account, 'mobile')
    )

    mother_name = fields.Field(
        column_name = 'mother_name',
        attribute = 'mother_name'
    )

    mother_contact_number = fields.Field(
        column_name = 'mother_contact_number',
        attribute = 'mother_contact_number'
    )
    
    mother_email = fields.Field(
        column_name = 'mother_email',
        attribute = 'mother_email'
    )

    date_created = fields.Field(
        column_name = 'date_created',
        attribute = 'date_created'
    )
    class Meta:
        model = Parent
        exclude = ('id',)
        import_id_fields=('father_email',)
        fields = ('father_email','father_name','father_contact_number','mother_email','date_created')

class TeacherResource(resources.ModelResource):
    teacher_email = fields.Field(
        column_name = 'teacher_email',
        attribute = 'teacher_email'
    )
    teacher_id = fields.Field(
        column_name = 'teacher_id',
        attribute = 'teacher_id'
    )
    first_name = fields.Field(
        column_name = 'first_name',
        attribute = 'first_name'
    )
    middle_name = fields.Field(
        column_name = 'middle_name',
        attribute = 'middle_name'
    )
    last_name = fields.Field(
        column_name = 'last_name',
        attribute = 'last_name'
    )
    contact_number = fields.Field(
        column_name = 'contact_number',
        attribute = 'contact_number',
        widget = ForeignKeyWidget(Account, 'mobile')
    )
    joining_date = fields.Field(
        column_name = 'joining_date',
        attribute = 'joining_date'
    )
    date_of_birth = fields.Field(
        column_name = 'date_of_birth',
        attribute = 'date_of_birth'
    )
    blood_group = fields.Field(
        column_name = 'blood_group',
        attribute = 'blood_group'
    )
    adhar_number = fields.Field(
        column_name = 'adhar_number',
        attribute = 'adhar_number'
    )
    country = fields.Field(
        column_name = 'country',
        attribute = 'country'
    )
    state = fields.Field(
        column_name = 'state',
        attribute = 'state'
    )
    city = fields.Field(
        column_name = 'city',
        attribute = 'city'
    )
    street = fields.Field(
        column_name = 'street',
        attribute = 'street'
    )
    date_created = fields.Field(
        column_name = 'date_created',
        attribute = 'date_created'
    )
    class Meta:
        model = Teacher
        exclude = ('id',)
        import_id_fields=('teacher_email',)
        fields = ('teacher_email','teacher_id','first_name','middle_name','last_name','contact_number','joining_date',
        'date_of_birth','blood_group','adhar_number','country','state','city','street','date_created')

class StudentResource(resources.ModelResource):
    id = fields.Field(
        column_name = 'id',
        attribute = 'id'
    )
    first_name = fields.Field(
        column_name = 'first_name',
        attribute = 'first_name'
    )
    middle_name = fields.Field(
        column_name = 'middle_name',
        attribute = 'middle_name'
    )
    last_name = fields.Field(
        column_name = 'last_name',
        attribute = 'last_name'
    )
    registration_number = fields.Field(
        column_name = 'registration_number',
        attribute = 'registration_number',
        widget = ForeignKeyWidget(Account, 'registration_number')
    )
    student_email = fields.Field(
        column_name = 'student_email',
        attribute = 'student_email'
    )
    roll_no = fields.Field(
        column_name = 'roll_no',
        attribute = 'roll_no'
    )
    blood_group = fields.Field(
        column_name = 'blood_group',
        attribute = 'blood_group'
    )
    adhar_number = fields.Field(
        column_name = 'adhar_number',
        attribute = 'adhar_number'
    )
    date_of_birth = fields.Field(
        column_name = 'date_of_birth',
        attribute = 'date_of_birth'
    )
    admission_date = fields.Field(
        column_name = 'admission_date',
        attribute = 'admission_date'
    )
    country = fields.Field(
        column_name = 'country',
        attribute = 'country'
    )
    state = fields.Field(
        column_name = 'state',
        attribute = 'state'
    )
    city = fields.Field(
        column_name = 'city',
        attribute = 'city'
    )
    street = fields.Field(
        column_name = 'street',
        attribute = 'street'
    )
    parent = fields.Field(
        column_name = 'parent',
        attribute = 'parent',
        widget = ForeignKeyWidget(Parent, 'father_email')
    )
    
    classes = fields.Field(
        column_name = 'classes',
        attribute = 'classes',
        widget = ForeignKeyWidget(ClassesSection, 'id')
    )
    date_created = fields.Field(
        column_name = 'date_created',
        attribute = 'date_created'
    )
    class Meta:
        model = Student
        fields = ('first_name','middle_name','last_name','registration_number','student_email',
        'roll_no','blood_group','adhar_number','date_of_birth','admission_date','country',
        'state','city','street','parent','classes','date_created')

class ParentAdmin(ImportExportModelAdmin,admin.ModelAdmin):
    resource_class = ParentResource
    list_display = ('father_contact_number','father_email','father_name','mother_name','mother_contact_number',
    'mother_email','date_created')
    search_fields = ('user','father_name',)
    readonly_fields = ('date_created',)

    filter_horizontal = ()
    list_filter = ['father_email']
    fieldsets = ()
    raw_id_fields = ('father_contact_number',)

class TeacherAdmin(ImportExportModelAdmin,admin.ModelAdmin):
    resource_class = TeacherResource
    list_display = ('contact_number','teacher_email','first_name','middle_name','last_name','joining_date',
    'blood_group','adhar_number','country','state','city', 'street','date_created')
    search_fields = ('user','first_name',)
    readonly_fields = ('date_created',)
    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

class StudentAdmin(ImportExportModelAdmin,admin.ModelAdmin):
    resource_class = StudentResource
    list_display = ('registration_number','first_name')
#     list_display = ('registration_number','student_email','first_name','middle_name','last_name','roll_no','blood_group',
#     'adhar_number','date_of_birth','admission_date','classes','country','state','city', 'street','parent','date_created')
#     readonly_fields = ('date_created',)
#     # raw_id_fields = ('registration_number',)

    
admin.site.register(Parent, ParentAdmin)
admin.site.register(Teacher, TeacherAdmin)
admin.site.register(Student, StudentAdmin)
