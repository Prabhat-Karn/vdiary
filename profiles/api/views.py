from rest_framework.views import APIView
from rest_framework.response import Response
from Vdiary.permissions import IsParent, IsTeacher, IsStudent, IsPrincipal
from rest_framework.permissions import IsAuthenticated
from django.views import View
from django.shortcuts import render, redirect

from profiles.api.serializers import TeacherProfileSerializer, ClassStudentSerializer, TeacherProfileByIdSerializer
from profiles.models import Teacher, Student
from account.models import AccountRole
from lecture.models import Lecture
from classes.models import ClassesSection

from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator

from rest_framework.authentication import SessionAuthentication, TokenAuthentication

from rest_framework.status import(
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from django.db.models import Q

class TeacherProfile(APIView):
    serializer_class = TeacherProfileSerializer
    authentication_classes = (SessionAuthentication,TokenAuthentication)
    def get_permissions(self):
        if self.request.method == 'GET':
            permission_classes = (IsAuthenticated, IsTeacher)
        else:
            permission_classes = (IsAuthenticated,IsPrincipal)
        return [permission() for permission in permission_classes]

    def get_role (self, request):
        # required_role = request.user.roles.all()
        try:
            return AccountRole.objects.filter(account = request.user)
        except AccountRole.DoesNotExist:
            return Response(status=HTTP_404_NOT_FOUND)

    def get_teacher (self, request):
        try:
            return Teacher.objects.filter(contact_number=request.user)
        except Teacher.DoesNotExist:
            return Response(status=HTTP_404_NOT_FOUND)

    def get_teacher_through_id(self, request):
        try:
            return Teacher.objects.filter(teacher_id = request.data['teacher_id'])
        except Teacher.DoesNotExist:
            return Response(status = HTTP_404_NOT_FOUND)

    def get_lecture (self, request):
        try:
            return Lecture.objects.filter(teacher__contact_number = request.user)
        except Lecture.DoesNotExist:
            return Response(status=HTTP_404_NOT_FOUND)

    def get(self, request, format = None):
        role = self.get_role(request)
        teacher = self.get_teacher(request)
        lecture = self.get_lecture(request)
        teacher_details = [{'teacher_detail':{'name':i.first_name+' '+str(i.middle_name)+' '+str(i.last_name),'email':i.teacher_email,'teacher_id':i.teacher_id, 'contact_number':str(i.contact_number),
            'joining_date':i.joining_date,'date_of_birth':i.date_of_birth,'blood_group':i.blood_group,'adhar_number':i.adhar_number,
            'country':i.country,'state':i.state,'city':i.city, 'street':i.street, 
            'date_created':i.date_created} for i in teacher},
            
            {'subject':{str(j.subject) for j in lecture}},{'role':{str(k.roles) for k in role}},
            {'class':{str(k.classes.classes)for k in role if not (k.classes is None) }},
            {'section':{str(k.classes.section)for k in role if not (k.classes is None)}}]
        return Response(teacher_details, status = HTTP_200_OK)
    
    def post(self, request, format = None):
        teacher = self.get_teacher_through_id(request)
        serializer = TeacherProfileByIdSerializer(teacher,context = {'request':request}, many = True)
        return Response(serializer.data, status = HTTP_200_OK)

class SpecificStudentDetail(APIView):
    serializer_class = TeacherProfileSerializer
    authentication_classes = (SessionAuthentication,TokenAuthentication)
    permission_classes = (IsAuthenticated,IsTeacher,)

    def post(self, request, format = None):
        student_details = []
        subject = request.data['subject']
        student_detail = Student.objects.filter(
            registration_number__registration_number = request.data['registration_number']
        ).values('first_name','middle_name','last_name','registration_number__registration_number',
        'roll_no','classes__classes','classes__section')

        student_details.append({'name':student_detail[0]['first_name']+' '+str(student_detail[0]['middle_name'])+' '+str(student_detail[0]['last_name']),
        'registration_number':student_detail[0]['registration_number__registration_number'],'roll_no':student_detail[0]['roll_no'], 'subject':subject,
        'class_section':str(student_detail[0]['classes__classes'])+' '+student_detail[0]['classes__section']})

        return Response(student_details, status = HTTP_200_OK)

class ClassStudent(APIView):
    serializer_class = ClassStudentSerializer
    authentication_classes = (SessionAuthentication,TokenAuthentication)
    permission_classes = (IsAuthenticated,IsTeacher)

    def get_classes(self, request):
        try:
            return ClassesSection.objects.filter(
                Q(classes = request.data['classes']) &
                Q(section = request.data['section'])
            ).values('id')
        except ClassesSection.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def get_object(self, request):
        try: 
            return Student.objects.filter(classes = self.get_classes(request)[0]['id'])
        except Student.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def post(self, request, format = None):
        # print(self.get_classes(request)[0]['id'])
        student = self.get_object(request)
        
        serializer = ClassStudentSerializer(student, many = True)
        return Response(serializer.data)

class EveryTeacherDetail(APIView):
    authentication_classes = (SessionAuthentication,TokenAuthentication)
    permission_classes = (IsAuthenticated,IsPrincipal)

    def get_teacher (self, request):
        try:
            return Teacher.objects.all()
        except Teacher.DoesNotExist:
            return Response(status=HTTP_404_NOT_FOUND)

    def get(self, request, format = None):
        teacher = self.get_teacher(request)
        serializer = TeacherProfileSerializer(teacher, many = True)
        return Response(serializer.data)

class StudentDetailRegParentStudent(APIView):
    authentication_classes = (SessionAuthentication,TokenAuthentication)
    def get_permissions(self):
        if self.request.method == 'GET':
            permission_classes = (IsAuthenticated, IsParent | IsStudent)
        else:
            permission_classes = (IsAuthenticated,IsTeacher)
        return [permission() for permission in permission_classes]

    def get_student_for_parent_student(self, request): #student detail by parent and student
        try:
            return Student.objects.filter(Q(parent__father_contact_number = request.user) | 
            Q(registration_number = request.user))
        except Student.DoesNotExist:
            return Response(status = HTTP_404_NOT_FOUND)

    def get_student (self, request):
        try:
            return Student.objects.filter(registration_number__registration_number = request.data['registration_number'])
        except Student.DoesNotExist:
            return Response(status=HTTP_404_NOT_FOUND)

    def get(self, request, format = None):
        student = self.get_student_for_parent_student(request)
        serializer = ClassStudentSerializer(student, many = True)
        return Response(serializer.data, status = HTTP_200_OK)

    def post(self, request, format = None): #student detail by teacher through registration number
        student = self.get_student(request)
        serializer = ClassStudentSerializer(student, many = True)
        return Response(serializer.data, status = HTTP_200_OK)