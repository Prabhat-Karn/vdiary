from rest_framework import serializers
from profiles.models import Teacher, Student
from account.models import AccountRole
from lecture.models import Lecture

class TeacherProfileSerializer(serializers.ModelSerializer):
    contact_number = serializers.CharField(source = 'contact_number.mobile', read_only = True)
    name = serializers.SerializerMethodField()
    def get_name(self, obj):
        return '{} {} {}'.format(obj.first_name, str(obj.middle_name or ''), str(obj.last_name or ''))
    class Meta:
        model = Teacher
        fields = ['teacher_email','teacher_id','name',
                    'contact_number','joining_date','blood_group','adhar_number',
                    'country','state','city','street','date_created']

class TeacherProfileByIdSerializer(serializers.ModelSerializer):
    contact_number = serializers.CharField(source = 'contact_number.mobile', read_only = True)
    name = serializers.SerializerMethodField()
    role = serializers.SerializerMethodField()
    classes = serializers.SerializerMethodField()
    section = serializers.SerializerMethodField()
    subject = serializers.SerializerMethodField()

    def teacher_id(self, obj):
        teacher_id = self.context['request'].data['teacher_id']
        return teacher_id

    def get_name(self, obj):
        return '{} {} {}'.format(obj.first_name, str(obj.middle_name or ''), str(obj.last_name or ''))

    def get_designation(self, obj):
        role = AccountRole.objects.filter(account__teacher__teacher_id = self.teacher_id(obj))
        return role

    def get_role(self,obj):
        return [str(k) for k in self.get_designation(obj)]

    def get_classes(self, obj):
        return [str(k.classes.classes) for k in self.get_designation(obj) if not (k.classes is None)]

    def get_section(self, obj):
        return [str(k.classes.section) for k in self.get_designation(obj) if not (k.classes is None)]

    def get_lecture(self, obj):
        lecture = Lecture.objects.filter(teacher__teacher_id = self.teacher_id(obj))
        return lecture
    
    def get_subject(self,obj):
        return set([str(k.subject) for k in self.get_lecture(obj)])
    class Meta:
        model = Teacher
        fields = ['teacher_email','teacher_id','name', 'contact_number', 'date_of_birth',
                'joining_date','blood_group','adhar_number','country',
                'state','city','street','date_created','role', 'classes','section','subject',]
            
class ClassStudentSerializer(serializers.ModelSerializer):
    # name = serializers.serializerMethodField()
    name = serializers.SerializerMethodField()
    father_email = serializers.CharField(source = 'parent.father_email')
    father_name = serializers.CharField(source = 'parent.father_name')
    father_contact_number = serializers.CharField(source = 'parent.father_contact_number')
    mother_name = serializers.CharField(source = 'parent.mother_name')
    mother_contact_number = serializers.CharField(source = 'parent.mother_contact_number')
    mother_email = serializers.CharField(source = 'parent.mother_email')
    classes = serializers.CharField(source = 'classes.classes')
    section = serializers.CharField(source = 'classes.section')
    registration_number = serializers.CharField(source = 'registration_number.registration_number')
    def get_name(self, obj):
        return '{} {} {}'.format(obj.first_name, str(obj.middle_name or ''), str(obj.last_name or ''))
    class Meta:
        model = Student
        fields = ['registration_number','roll_no','name', 'student_email','blood_group','adhar_number',
        'date_of_birth','admission_date','country','state','city','street','father_name','father_contact_number',
        'father_email','mother_name','mother_email','mother_contact_number','classes','section','date_created']