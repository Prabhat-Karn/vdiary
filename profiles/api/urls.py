from django.urls import path
from profiles.api import views

# from django.contrib.auth.decorators import login_required

app_name = 'profiles'


urlpatterns = [
    path('get_teacher/',views.TeacherProfile.as_view()),
    path('student_detail/',views.SpecificStudentDetail.as_view()),
    path('class_student/',views.ClassStudent.as_view()),
    path('get_all_teacher/',views.EveryTeacherDetail.as_view()), #get details of all teacher for principal
    path('get_student_reg_ps/', views.StudentDetailRegParentStudent.as_view()), #retrive student details with token if parent or student else with reg_no
]