from django.db import models
from classes.models import ClassesSection
from django.conf import settings
from account.models import Account
# Create your models here.
class Parent(models.Model):
    father_email = models.EmailField(max_length=254, primary_key = True)
    father_name = models.CharField(max_length=50)
    father_contact_number = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, unique = True)
    mother_name = models.CharField(max_length=50)
    mother_contact_number = models.CharField(max_length=15)
    mother_email = models.EmailField(max_length=60)
    date_created = models.DateTimeField(verbose_name = "date created", auto_now_add=True)

    def __str__(self):
        return str(self.father_email)
    


class Teacher(models.Model):
    teacher_email = models.EmailField(max_length=254, primary_key = True)
    teacher_id = models.CharField(max_length=50, unique= True)
    first_name = models.CharField(max_length = 30)
    middle_name = models.CharField(max_length = 30, blank = True, null= True)
    last_name = models.CharField(max_length = 30, blank = True, null = True)
    contact_number  = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, unique = True)
    joining_date = models.DateField(auto_now=False, auto_now_add=False)
    date_of_birth = models.DateField(auto_now=False, auto_now_add=False)
    blood_group = models.CharField(max_length=15)
    adhar_number = models.CharField(max_length=50)
    country = models.CharField(max_length=50)
    state = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    street = models.CharField(max_length=50)
    date_created = models.DateTimeField(verbose_name = "date created", auto_now_add=True)

    def __str__(self):
        return str(self.teacher_email)
    

class Student(models.Model):
    first_name = models.CharField(max_length=50)
    middle_name = models.CharField(max_length=50, blank = True, null= True)
    last_name = models.CharField(max_length=50, blank = True, null=True)
    registration_number = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, unique = True)
    student_email = models.EmailField(max_length=254, blank = True, null=True)
    roll_no = models.CharField(max_length=3)
    # contact_number = models.CharField(max_length=15, blank = True)
    blood_group = models.CharField(max_length=50)
    adhar_number = models.CharField(max_length=50)
    date_of_birth = models.DateField(auto_now=False, auto_now_add=False)
    admission_date = models.DateField(auto_now=False, auto_now_add=False)
    country = models.CharField(max_length=50)
    state = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    street = models.CharField(max_length=50)
    parent = models.ForeignKey(Parent, on_delete=models.CASCADE)
    classes = models.ForeignKey(ClassesSection, on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.registration_number)