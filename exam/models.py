from django.db import models
from subject.models import Subject
from classes.models import ClassesSection
from profiles.models import Student
# Create your models here.
class TestTypes(models.Model):
    test_types = models.CharField(max_length=50)

    def __str__(self):
        return self.test_types
    
class Result(models.Model):
    # TEST_TYPES = (
    #     ('Class Test', 'Class Test'),
    #     ('Semester Test', 'Semester Test'),
    #     ('Annual Test','Annual Test'),
    # )
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    classes = models.ForeignKey(ClassesSection, on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    # test_types = models.CharField(max_length=14, choices = TEST_TYPES)
    test_types = models.ForeignKey(TestTypes, on_delete=models.CASCADE)
    marks_obtained = models.PositiveSmallIntegerField()
    total_marks = models.PositiveSmallIntegerField()
    pass_marks = models.PositiveSmallIntegerField(default = None)
    percentage = models.CharField(max_length=5)
    remarks =  models.BooleanField()
    test_date = models.DateField(auto_now=False, auto_now_add=False)
    date_created = models.DateField(auto_now_add=True)