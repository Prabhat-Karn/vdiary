from django.urls import path
from exam.api import views

app_name = 'exam'


urlpatterns = [
    path('exam_result/', views.ResultView.as_view(), name = 'result'),
    path('get_result/', views.GetResultView.as_view(), name = 'result'),
    path('get_date/', views.DateAndTestType.as_view(), name = 'date and test types'),
    path('test_types/', views.TestType.as_view(), name = 'test types'),
    path('get_student_result_sub/',views.GetResultEverySub.as_view(),name = 'get student result'), #student result of every subject for classteacher
]