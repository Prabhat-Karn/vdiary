from rest_framework import serializers
from exam.models import Result, TestTypes
class ResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = Result
        fields = ['student','classes', 'subject', 'test_types', 'marks_obtained','total_marks','pass_marks','remarks','test_date','percentage']


class GetResultSerializer(serializers.ModelSerializer):
    subject = serializers.CharField(source = 'subject.subject_name', read_only = True)
    classes = serializers.CharField(source = 'classes.classes',read_only = True)
    section = serializers.CharField(source = 'classes.section',read_only = True)
    student = serializers.CharField(source = 'student.registration_number.registration_number',read_only = True)
    roll_no = serializers.CharField(source = 'student.roll_no',read_only = True)
    name = serializers.SerializerMethodField()
    test_type = serializers.CharField(source = 'test_types.test_types', read_only = True)
    def get_name(self, obj):
        return '{} {} {}'.format(obj.student.first_name, obj.student.middle_name, obj.student.last_name)
    
    class Meta:
        model = Result
        fields =  ['id','roll_no','name','student','subject','classes','section','test_type','marks_obtained','total_marks','pass_marks','remarks','percentage']

class GetDateSerializer(serializers.ModelSerializer):
    test_type = serializers.CharField(source = 'test_types.test_types', read_only = True)
    class Meta:
        model = Result
        fields = ['id','test_date','test_type']

class TestTypesSerializer(serializers.ModelSerializer):
    class Meta: 
        model = TestTypes
        fields = ['test_types']