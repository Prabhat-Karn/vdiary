from rest_framework.views import APIView
from rest_framework.response import Response
from Vdiary.permissions import IsParent, IsTeacher, IsStudent, IsPrincipal, IsClassTeacher
from rest_framework.permissions import IsAuthenticated
from rest_framework.status import(
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from django.core.exceptions import ValidationError

from exam.models import Result, TestTypes
from profiles.models import Student
from  subject.models import Subject
from classes.models import ClassesSection
from exam.api.serializers import ResultSerializer, GetResultSerializer, GetDateSerializer, TestTypesSerializer

from django.db.models import Q

from django.views import View
from django.shortcuts import render, redirect

from rest_framework.authentication import SessionAuthentication, TokenAuthentication

import json
class TestType(APIView):
    permission_classes = (IsAuthenticated,IsTeacher | IsParent | IsStudent)
    def get_object(self, request):
        try:
            return TestTypes.objects.all().values('test_types')
        except TestTypes.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, format = None):
        testtypes = self.get_object(request)
        serializer = TestTypesSerializer(testtypes, many = True)
        return Response(serializer.data)
class DateAndTestType(APIView):
    authentication_classes = (SessionAuthentication,TokenAuthentication)
    
    def get_permissions(self):
        if self.request.method == 'GET':
            permission_classes = (IsAuthenticated,IsParent | IsStudent,)
        else:
            permission_classes = (IsAuthenticated,IsTeacher,)
        return [permission() for permission in permission_classes]

    def get_object(self, request):
        try:
            return Result.objects.filter(Q(subject__subject_name = request.data['subject'])&
            Q(classes__classes = request.data['classes'])&Q(classes__section = request.data['section'])&
            Q(test_types__test_types = request.data['test_types']))
        except Result.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def post(self, request, format = None):
        dates = self.get_object(request)
        serializer = GetDateSerializer(dates, many = True)
        return Response(serializer.data, status = HTTP_200_OK)

        

class ResultView(APIView):

    serializer_class = ResultSerializer
    authentication_classes = (SessionAuthentication,TokenAuthentication)
    
    def get_permissions(self):
        if self.request.method == 'GET':
            permission_classes = (IsAuthenticated,IsParent | IsStudent,)
        else:
            permission_classes = (IsAuthenticated,IsTeacher,)
        return [permission() for permission in permission_classes]

    # def get_object(self, request):
    #     try:
    #         return Result.objects.filter(Q(subject__subject_name = request.data['subject'])&
    #         Q(classes__classes = request.data['classes'])&Q(classes__section = request.data['section'])&
    #         test_types = request.data['test_types'])
    #     except Result.DoesNotExist:
    #         return Response(status=status.HTTP_404_NOT_FOUND)

    def post(self, request, format = None):
        for i in json.loads(request.body):
            student = i['student']
            # classes = i['classes']
            # section = i['section']
            # subject = i['subject']
            test_types = i['test_types']
            marks_obtained = i['marks_obtained']
            total_marks = i['total_marks']
            pass_marks = i['pass_marks']
            test_date = i['test_date']
            if int(marks_obtained) > int(pass_marks):
                remarks = True
            else:
                remarks = False
            percentage = int(marks_obtained)/int(total_marks)*100

            sub_id = Subject.objects.filter(Q(subject_name = i['subject'])&
            Q(classes__classes = i['classes'])&
            Q(classes__section =i['section'])).values('id')

            classes_id = ClassesSection.objects.filter(Q(classes = i['classes'])& 
            Q(section = i['section'])).values('id')

            student_id = Student.objects.filter(registration_number__registration_number = i['student']).values('id')
            
            test_types_id = TestTypes.objects.filter(test_types = test_types).values('id')
        
            serializer = ResultSerializer(data = {"student":student_id[0]['id'],"classes":classes_id[0]['id'],
                                            "subject":sub_id[0]['id'],"test_types":test_types_id[0]['id'],"marks_obtained":marks_obtained,
                                            "total_marks":total_marks,"pass_marks":pass_marks, "remarks":remarks, "test_date":test_date,
                                            "percentage":percentage
                                        })
            data = {}
            if serializer.is_valid():
                result = serializer.save()
                data['response'] = 'Result Published!'

            else:
                data = serializer.errors
        return Response(data)

    def validate_ids(self, id_list):
            for id in id_list:
                try:
                    Result.objects.get(id=id)
                except (Result.DoesNotExist, ValidationError):
                    raise HTTP_400_BAD_REQUEST
            return True
    def get_obj(self, obj_id):
        try:
            return Result.objects.get(id = obj_id)
        except (Result.DoesNotExist, ValidationError):
            raise HTTP_400_BAD_REQUEST
    def put(self, request, format = None):
        data = request.data
        result_ids = [i['id'] for i in data]
        self.validate_ids(result_ids)
        for res_dict in data:
            result_id = res_dict['id']
            marks_obtained = res_dict['marks_obtained']
            pass_marks = res_dict['pass_marks']
            total_marks = res_dict['total_marks']
            percentage = int(marks_obtained)/int(total_marks)*100
            if int(marks_obtained) > int(pass_marks):
                remarks = True
            else:
                remarks = False
            
            obj = self.get_obj(result_id)
            obj.marks_obtained = marks_obtained
            obj.pass_marks = pass_marks
            obj.total_marks = total_marks
            obj.percentage = percentage
            obj.remarks = remarks
            obj.save()
        return Response({'Response':'Result Updated'}, status = HTTP_200_OK)

class GetResultView(APIView):

    serializer_class = ResultSerializer
    authentication_classes = (SessionAuthentication,TokenAuthentication)
    
    def get_permissions(self):
        if self.request.method == 'GET':
            permission_classes = (IsAuthenticated,IsParent | IsStudent,)
        else:
            permission_classes = (IsAuthenticated,IsTeacher,)
        return [permission() for permission in permission_classes]

    def get_object(self, request):
        try:
            return Result.objects.filter(Q(subject__subject_name = request.data['subject'])&
            Q(classes__classes = request.data['classes'])&Q(classes__section = request.data['section'])&
            Q(test_types__test_types = request.data['test_types']) & Q(test_date = request.data['test_date']))
        except Result.DoesNotExist:
            return Response(status=HTTP_404_NOT_FOUND)

    def post(self, request, format = None):
        result_details = self.get_object(request)
        serializer = GetResultSerializer(result_details, many = True)
        return Response(serializer.data, status = HTTP_200_OK)

class GetResultEverySub(APIView):
    permission_classes = (IsAuthenticated,IsPrincipal | IsClassTeacher | IsParent | IsStudent)
    # def get_subject(self, request):
    #     try:
    #         return Subject.objects.filter(classes__student__registration_number__registration_number = request.data['registration_number']).values('id')
    #     except Subject.DoesNotExist:
    #         return Response(status = HTTP_404_NOT_FOUND)
    def get_result(self, request):
        # subjects = list(self.get_subject(request))
        return Result.objects.filter(Q(student__registration_number__registration_number = request.data['registration_number'])&
            Q(test_types__test_types = request.data['test_types']))
        # for i in subjects:
            # result =  Result.objects.filter(Q(subject = str(i.id)) & 
            # Q(student__registration_number__registration_number = request.data['registration_number'])&
            # Q(test_types__test_types = request.data['test_types']))
        # return result
    def post(self, request, format = None):
        results = self.get_result(request)
        if results.exists():
            serializer = GetResultSerializer(results, many = True)
            return Response(serializer.data, status = HTTP_200_OK)
        else:
            return Response(status = HTTP_404_NOT_FOUND)
