from django.contrib import admin
from exam.models import Result, TestTypes
# Register your models here.
class ResultAdmin(admin.ModelAdmin):
    list_display = ('student','classes','subject','test_types','total_marks','marks_obtained','pass_marks','percentage','remarks','test_date','date_created')

class TestTypesAdmin(admin.ModelAdmin):
    list_display = ('id','test_types')

admin.site.register(Result, ResultAdmin)
admin.site.register(TestTypes, TestTypesAdmin)