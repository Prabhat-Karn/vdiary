from django.contrib.auth import get_user_model
from rest_framework import permissions
from account.models import Role, AccountRole
# from rest_framework.permissions import IsAuthenticated
User = get_user_model()

class IsParent(permissions.BasePermission):

    def has_permission(self, request, view):
        required_role = request.user.roles.all()
        for i in required_role:
            if str(i) == Role.Parent:
                return True
        return False

class IsTeacher(permissions.BasePermission):

    def has_permission(self, request, view):
        required_role = request.user.roles.all()
        for i in required_role:
            if str(i) == Role.Teacher:
                return True
        return False

class IsStudent(permissions.BasePermission):

    def has_permission(self, request, view):
        required_role = request.user.roles.all()
        for i in required_role:
            if str(i) == Role.Student:
                return True
        return False

class IsPrincipal(permissions.BasePermission):

    def has_permission(self, request, view):
        required_role = request.user.roles.all()
        for i in required_role:
            if str(i) == Role.Principal:
                return True
        return False

class IsClassTeacher(permissions.BasePermission):

    def has_permission(self, request, view):
        required_role = request.user.roles.all()
        for i in required_role:
            if str(i) == Role.Class_Teacher:
                return True
        return False