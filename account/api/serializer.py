from rest_framework import serializers
from account.models import AccountRole

class TokenSerializer(serializers.Serializer):
    mobile = serializers.CharField(required = True, max_length = 60)
    # roles = serializers.CharField(required = True, max_length = 14)
    password = serializers.CharField(required = True, style = {'input_type':'password'})