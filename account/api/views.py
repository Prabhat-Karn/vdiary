from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from django.contrib.auth import get_user_model, authenticate
from rest_framework.authtoken.models import Token
from account.api.serializer import TokenSerializer
from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated

from account.models import AccountRole

from django.views import View
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)


class Login(APIView):
    serializer_class = TokenSerializer

    def get(self, request, format = None):
        serializer = TokenSerializer()
        return Response(status = status.HTTP_200_OK)

    def post(self, request, format = None):
        serializer = TokenSerializer(data = request.data)
        if serializer.is_valid():
            user = authenticate(
                username = serializer.data.get('mobile'),
                password = serializer.data.get('password')
            )

            if user is not None:
                token, create_or_fetch = Token.objects.get_or_create(
                    user = user
                )
                role_list = []
                role = AccountRole.objects.filter(account = user)

                for i in range(len(role)):
                    if str(role[i]) == request.data.get('roles'):
                        return Response({'token':token.key, 'roles':str(role[i])}, status=status.HTTP_200_OK)
                    else:
                        continue
            message = 'Wrong credentials. Please try again'
            return Response({'message':message},status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

class Logout(APIView):
    permission_classes = (IsAuthenticated,)
    def get (self, request, format = None):
        request.user.auth_token.delete()
        return Response ({'message':'Logged Out'},status = HTTP_200_OK)

