from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from classes.models import ClassesSection

# Create your models here.
class Role(models.Model):
    Administration = 'Administration'
    Principal = 'Principal'
    Parent = 'Parent'
    Teacher = 'Teacher'
    Class_Teacher = 'Class Teacher'
    Student = 'Student'
    Role_Choices = (
        (Administration,'Administration'),
        (Principal, 'Principal'),
        (Parent, 'Parent'),
        (Teacher,'Teacher'),
        (Class_Teacher, 'Class Teacher'),
        (Student, 'Student'),
    )
    role = models.CharField(max_length=14, choices = Role_Choices)

    def __str__(self):
        return self.role

class MyAccountManager(BaseUserManager):
    def create_user(self, mobile, username, password=None):
        if not mobile:
            raise ValueError('User must have a mobile number')
        if not username:
            raise ValueError('User must have a username')
        # if not roles:
        #     raise ValueError('User must have a role')
        user = self.model(
            mobile = mobile,
            username = username,
            # roles = roles
        ) 
        user.set_password(password)
        user.save(using = self._db)
        return user

    def create_superuser(self, mobile, username, password):
       
        user = self.create_user(
            mobile = mobile,
            password = password,
            username = username,
        )
        user.is_staff = True
        user.is_superuser = True
        user.save(using = self._db)

        return user


class Account(AbstractBaseUser):
    mobile = models.CharField(verbose_name = 'mobile', max_length=16,  null = True, blank = True, unique = True)
    registration_number = models.CharField(max_length=50, null = True, blank = True, unique = True)
    username = models.CharField(max_length = 30, unique = True)
    date_joined = models.DateTimeField(verbose_name = "date joined", auto_now_add=True)
    last_login = models.DateTimeField(verbose_name = "last login", auto_now=True)
    is_active = models.BooleanField(default =  True)
    is_staff = models.BooleanField(default =  False)
    is_superuser = models.BooleanField(default =  False)

    roles = models.ManyToManyField(Role, through = 'AccountRole')

    USERNAME_FIELD = 'mobile'
    REQUIRED_FIELDS = ['username',]

    objects = MyAccountManager()

    def __str__(self):
        if not self.registration_number == None:
            return str(self.registration_number)
        else:
            return str(self.mobile)

    def has_perm(self, perm, obj = None):
        return True

    def has_module_perms(self, app_label):
        return True

class AccountRole(models.Model):
    roles = models.ForeignKey(Role, on_delete=models.CASCADE)
    account = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    classes = models.ForeignKey(ClassesSection, blank = True, null = True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.roles)
    


# @receiver(post_save, sender = settings.AUTH_USER_MODEL)
# def create_auth_token(sender, instance = None, created=False, **kwargs):
#     if created:
#         Token.objects.create(user=instance)
