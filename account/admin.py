from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from account.models import Account, Role, AccountRole
from classes.models import ClassesSection
from import_export.admin import ImportExportModelAdmin
from import_export import fields, resources
from import_export.widgets import ForeignKeyWidget
from django.contrib.auth.hashers import make_password
from tablib import Dataset
from django.db.models import Q
# Register your models here.
class UserResource(resources.ModelResource):
    def before_import_row(self, row, **kwargs):
        value = str(row['password'])
        row['password'] = make_password(value)
    class Meta:
        model = Account

class AccountRoleResource(resources.ModelResource):
    id = fields.Field(
        column_name = 'id',
        attribute = 'id'
    )
    roles = fields.Field(
        column_name = 'roles',
        attribute = 'roles',
        widget = ForeignKeyWidget(Role, 'role')
    )
    
    account = fields.Field(
        column_name = 'username',
        attribute = 'account',
        widget = ForeignKeyWidget(Account, 'username')
    )

    classes = fields.Field(
        column_name = 'classes_id',
        attribute = 'classes',
        widget = ForeignKeyWidget(ClassesSection, 'id')
    )

    class Meta:
        model = AccountRole
        fields = ('id','roles','account','classes',)

class AccountAdmin(ImportExportModelAdmin,UserAdmin):
    resource_class = UserResource
    list_display = ('id','registration_number','mobile','username','date_joined', 'last_login','is_active','is_staff','is_superuser')
    search_fields = ('mobile','registration_number',)
    readonly_fields = ('date_joined','last_login',)

    filter_horizontal = ()
    list_filter = ('registration_number', 'mobile',)
    fieldsets = ()


    add_fieldsets = (
        (None,{
            'fields': ('mobile', 'username','registration_number', 'password1', 'password2'),
        }),
    )

class RoleAdmin(admin.ModelAdmin):
    list_display = ('id','role')
    search_fields = ('id','role')
    
    filter_horizontal = ()
    list_filter =  ()
    fieldsets = ()

class AccountRoleAdmin(ImportExportModelAdmin,admin.ModelAdmin):
    resource_class = AccountRoleResource
    list_display = ('id','roles','account','classes')
    search_fields = ('roles',)
    
    filter_horizontal = ()
    list_filter =  ()
    fieldsets = ()
    raw_id_fields = ('account','classes')

admin.site.register(Account, AccountAdmin)
admin.site.register(Role, RoleAdmin)
admin.site.register(AccountRole, AccountRoleAdmin)