from django.contrib.auth.backends import ModelBackend
from account.models import Account

class MyBackend(ModelBackend):

    def authenticate(self, request, **kwargs):
        registration_number = kwargs['username']
        password = kwargs['password']
        try:
            student = Account.objects.get(registration_number=registration_number)
            if student.check_password(password) is True:
                return student
        except Account.DoesNotExist:
            pass