from django.db import models
from django.conf import settings
from classes.models import ClassesSection
# Create your models here.
class Remarks(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField(blank = True, null = True)
    sender = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name = 'sender')
    send_to = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name = 'send_to')
    date_created = models.DateField(verbose_name = "date created", auto_now_add=True)
    time_created = models.TimeField(verbose_name = "time created", auto_now_add=True)

    def __str__(self):
        return self.title

class Notice(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField(blank = True, null = True)
    classes = models.ForeignKey(ClassesSection, on_delete=models.CASCADE)
    date_created = models.DateField(verbose_name = "date created", auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

class Event(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField(blank = True, null = True)
    classes = models.ForeignKey(ClassesSection, on_delete=models.CASCADE)
    date_created = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

class Holiday(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField(blank = True, null = True)
    classes = models.ForeignKey(ClassesSection, on_delete=models.CASCADE)
    date_created = models.DateField( auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return self.title