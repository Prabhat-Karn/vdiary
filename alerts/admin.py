from django.contrib import admin
from alerts.models import Remarks, Notice, Event, Holiday
# Register your models here.
class RemarksAdmin(admin.ModelAdmin):
    list_display = ('title','sender','send_to','date_created','time_created')
    readonly_fields = ('date_created',)
    
    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

class NoticeAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'classes', 'date_created', 'created_by')
    readonly_fields = ('date_created',)
    
    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

class EventAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'classes', 'date_created', 'created_by')
    readonly_fields = ('date_created',)
    
    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

class HolidayAdmin(admin.ModelAdmin):
    list_display = ('title', 'classes', 'date_created', 'created_by')
    readonly_fields = ('date_created',)
    
    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

admin.site.register(Remarks,RemarksAdmin)
admin.site.register(Notice, NoticeAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(Holiday, HolidayAdmin)