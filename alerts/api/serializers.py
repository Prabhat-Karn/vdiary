from rest_framework import serializers
from alerts.models import Remarks, Notice, Event, Holiday
from account.models import AccountRole
# from profiles.models import Student

class RemarksSerializer(serializers.ModelSerializer):
    sender = serializers.SerializerMethodField('get_sender')
    send_to = serializers.SerializerMethodField('get_receiver')
    class Meta:
        model = Remarks
        fields = ['title','description','sender','send_to']

    def get_sender(self, Remarks):
        sender = str(Remarks.sender)
        return sender
    
    def get_receiver(self, Remarks):
        send_to = str(Remarks.send_to)
        return send_to

class GetRemarksSerializer(serializers.ModelSerializer):
    sender = serializers.SerializerMethodField()
    send_to = serializers.SerializerMethodField()
    def get_sender(self, obj):
        user = self.context['request'].user
        role = AccountRole.objects.filter(account = user)
        sent = self.context['request'].data['filter_by'] == 'sent'
        received = self.context['request'].data['filter_by'] == 'received'
        for i in range(len(role)):
            if str(role[i]) == 'Parent' and sent:
                return '{} {} {}'.format(obj.sender.student.first_name, str(obj.sender.student.middle_name or ''), str(obj.sender.student.last_name or ''))

            elif str(role[i]) == 'Parent' and received:
                return '{} {} {}'.format(obj.sender.teacher.first_name, str(obj.sender.teacher.middle_name or ''), str(obj.sender.teacher.last_name or ''))
            
            elif str(role[i]) == 'Teacher' and sent:
                return '{} {} {}'.format(obj.sender.teacher.first_name, str(obj.sender.teacher.middle_name or ''), str(obj.sender.teacher.last_name or ''))

            elif str(role[i]) == 'Teacher' and received:
                return '{} {} {}'.format(obj.sender.student.first_name, str(obj.sender.student.middle_name or ''), str(obj.sender.student.last_name or ''))

            elif str(role[i]) == 'Student' and received:
                return '{} {} {}'.format(obj.sender.teacher.first_name, str(obj.sender.teacher.middle_name or ''), str(obj.sender.teacher.last_name or ''))
            
            elif str(role[i]) == 'Student' and sent:
                return '{} {} {}'.format(obj.sender.student.first_name, str(obj.sender.student.middle_name or ''), str(obj.sender.student.last_name or ''))



    def get_send_to(self, obj):
        user = self.context['request'].user
        role = AccountRole.objects.filter(account = user)
        sent = self.context['request'].data['filter_by'] == 'sent'
        received = self.context['request'].data['filter_by'] == 'received'
        for i in range(len(role)):
            if str(role[i]) == 'Teacher' and received:
                return '{} {} {}'.format(obj.send_to.teacher.first_name, str(obj.send_to.teacher.middle_name or ''), str(obj.send_to.teacher.last_name or ''))
            
            elif str(role[i]) == 'Teacher' and sent:
                return '{} {} {}'.format(obj.send_to.student.first_name, str(obj.send_to.student.middle_name or ''), str(obj.send_to.student.last_name or ''))

            elif str(role[i]) == 'Parent' and sent:
                return '{} {} {}'.format(obj.send_to.teacher.first_name, str(obj.send_to.teacher.middle_name or ''), str(obj.send_to.teacher.last_name or ''))
                
            elif str(role[i]) == 'Parent' and received:
                return '{} {} {}'.format(obj.send_to.student.first_name, str(obj.send_to.student.middle_name or ''), str(obj.send_to.student.last_name or ''))

            elif str(role[i]) == 'Student' and sent:
                return '{} {} {}'.format(obj.send_to.teacher.first_name, str(obj.send_to.teacher.middle_name or ''), str(obj.send_to.teacher.last_name or ''))

            elif str(role[i]) == 'Student' and received:
                return '{} {} {}'.format(obj.send_to.student.first_name, str(obj.send_to.student.middle_name or ''), str(obj.send_to.student.last_name or ''))

    class Meta:
        model = Remarks
        fields = ['title','description','sender','send_to','date_created','time_created']

class NoticeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Notice
        fields = ['title','description','classes']

class GetNoticeSerializer(serializers.ModelSerializer):
    classes = serializers.CharField(source = 'classes.classes', read_only = True)
    section = serializers.CharField(source = 'classes.section', read_only = True)
    class Meta:
        model = Notice
        fields = ['title','description','classes','section','date_created']

class EventSerializer(serializers.ModelSerializer):

    class Meta:
        model = Event
        fields = ['title','description','classes']

class GetEventSerializer(serializers.ModelSerializer):
    classes = serializers.CharField(source = 'classes.classes', read_only = True)
    section = serializers.CharField(source = 'classes.section', read_only = True)
    class Meta:
        model = Event
        fields = ['title','description','classes','section','date_created']

class HolidaySerializer(serializers.ModelSerializer):

    class Meta:
        model = Holiday
        fields = ['title','description','classes']

class GetHolidaySerializer(serializers.ModelSerializer):
    classes = serializers.CharField(source = 'classes.classes', read_only = True)
    section = serializers.CharField(source = 'classes.section', read_only = True)
    class Meta:
        model = Holiday
        fields = ['title','classes', 'section','date_created']