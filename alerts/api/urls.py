from django.urls import path
from alerts.api import views

app_name = 'remarks'


urlpatterns = [
    path('remarks/', views.RemarksView.as_view(), name = 'remarks'),
    path('get_remarks/',views.GetRemarks.as_view(),name = 'get remarks'),
    path('notice/',views.NoticeView.as_view(), name = 'notice'),
    path('event/',views.EventView.as_view(), name = 'event'),
    path('holiday/',views.HolidayView.as_view(), name = 'holiday'),
    path('notice_date/',views.NoticeViewDateFilter.as_view(), name = 'notice date filter'),
    path('event_date/',views.EventViewDateFilter.as_view(), name = 'event date filter'),
    path('holiday_date/',views.HolidayViewDateFilter.as_view(), name = 'holiday date filter'),
    
]