from rest_framework.views import APIView
from rest_framework.response import Response
from Vdiary.permissions import IsParent, IsTeacher, IsStudent, IsPrincipal
from rest_framework.permissions import IsAuthenticated

from alerts.models import Remarks, Notice, Event, Holiday
from alerts.api.serializers import (
    RemarksSerializer, 
    GetRemarksSerializer, 
    NoticeSerializer,
    GetNoticeSerializer, 
    EventSerializer,
    GetEventSerializer, 
    HolidaySerializer,
    GetHolidaySerializer,
)
from account.models import Role, Account, AccountRole

from django.shortcuts import render, redirect
from django.views import View

from django.db.models import Q

from profiles.models import Student, Teacher, Parent
from classes.models import ClassesSection
from rest_framework.authentication import SessionAuthentication, TokenAuthentication

import json

from django.db.models.query import QuerySet

from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)

class RemarksView(APIView):
    serializer_class = RemarksSerializer
    authentication_classes = (SessionAuthentication,TokenAuthentication)
    def get_permissions(self):
        if self.request.method == 'GET':
            permission_classes = (IsAuthenticated,IsParent | IsStudent | IsTeacher,)
        else:
            permission_classes = (IsAuthenticated,IsTeacher | IsParent)
        return [permission() for permission in permission_classes]

    # def get_object(self, request):
    #     try:
    #         return Remarks.objects.filter(
    #         # Q(date_created__range=[request.data['from_date'],request.data['to_date']])&
    #         Q(send_to__mobile = request.user) | 
    #         Q(send_to__student__parent__father_contact_number = request.user) |
    #         Q(send_to__registration_number = request.user)).distinct()
    #     except Remarks.DoesNotExist:
    #         return Response(status=status.HTTP_404_NOT_FOUND)

    # def get(self, request, pk = None, format = None):
    #     remarks = self.get_object(request)
    #     serializer = RemarksSerializer(remarks, many = True)
    #     return Response(serializer.data)

    def post(self, request, format = None):
        bdy = json.loads(request.body)
        data = {}
        for i in range(len(bdy)):
            serializer = RemarksSerializer(data = {'title':bdy[i]['title']})

            acc_role = AccountRole.objects.filter(Q(account__mobile =bdy[i]['receiver']) | Q(account__registration_number = bdy[i]['receiver'])).values('roles__role')
            sender_role = AccountRole.objects.filter(Q(account =request.user))
            if str(sender_role[0]) == 'Parent':
                sender = Account.objects.get(registration_number = bdy[i]['sender'])
            else:
                sender = request.user
            if acc_role[0]['roles__role'] == 'Student':
                queryset = Account.objects.get(registration_number = bdy[i]['receiver'])
            else:
                queryset =  Account.objects.get(mobile = bdy[i]['receiver'])

            if serializer.is_valid():
                remarks = serializer.save(sender = sender, send_to = queryset)
                data['response'] = 'Remarks Sent'
            else:
                data = serializer.errors
        
        return Response(data)

class GetRemarks(APIView):
    permission_classes = (IsAuthenticated,IsParent | IsTeacher | IsStudent)
    def get_object(self, request):
        try:
            if request.data['filter_by'] == 'received':
                return Remarks.objects.filter(
                (Q(send_to__mobile = request.user) | 
                Q(send_to__student__parent__father_contact_number = request.user) |
                Q(send_to__registration_number = request.user)) &
                Q(date_created__range = [request.data['from_date'],request.data['to_date']])).distinct()
            
            elif request.data['filter_by'] == 'sent':
                return Remarks.objects.filter(
                (Q(sender__mobile = request.user) | 
                Q(sender__student__parent__father_contact_number = request.user) |
                Q(sender__registration_number = request.user)) &
                Q(date_created__range = [request.data['from_date'],request.data['to_date']])).distinct()
            else:
                return Response({'message':'Filter is not given'}, status = HTTP_200_OK)

        except Remarks.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def post(self, request, format = None):
        remarks_details = self.get_object(request)
        if isinstance(remarks_details, QuerySet) == True:
            serializer = GetRemarksSerializer(remarks_details, context = {'request':request}, many = True)
            return Response(serializer.data, status = HTTP_200_OK)
        else:
            return remarks_details

class NoticeView(APIView):
    def get_permissions(self):
        if self.request.method == 'GET':
            permission_classes = (IsAuthenticated,IsParent | IsStudent | IsTeacher,)
        else:
            permission_classes = (IsAuthenticated,IsPrincipal)
        return [permission() for permission in permission_classes]

    def post(self, request, format = None):
        data = {}
        for i in range(len(request.data)):
            classes_id = ClassesSection.objects.filter(Q(classes = request.data[i]['classes'])&
            Q(section = request.data[i]['section'])).values('id')
            serializer = NoticeSerializer(data = {"title": request.data[i]['title'],"classes":classes_id[0]['id'], "description":request.data[i]['description']})
            if serializer.is_valid():
                serializer.save(created_by = request.user)
                data['response'] = 'Notice Created'
            else:
                data = serializer.errors
        return Response(data)

        #     return Response({"Response":"Notice Created"}, status = HTTP_200_OK)
        # return Response(serializer.errors, status=HTTP_404_NOT_FOUND)
    def get_specific_object(self, request):
        try:
            return Notice.objects.filter(
               Q(classes__student__registration_number = request.user) |
               Q(classes__student__parent__father_contact_number = request.user)
            )
        except Notice.DoesNotExist:
            return Response(status = HTTP_404_NOT_FOUND)
    def get_object(self, request):
        try:
            return Notice.objects.all()
        except Notice.DoesNotExist:
            return Response(status = HTTP_404_NOT_FOUND)
    def get(self, request, format = None):
        required_role = request.user.roles.all()
        if required_role.filter(Q(role='Student') | Q(role = 'Parent')).exists():
            notice = self.get_specific_object(request)
        else:
            notice = self.get_object(request)
        serializer = GetNoticeSerializer(notice, many = True)
        return Response(serializer.data)

class NoticeViewDateFilter(APIView):
    permission_classes = (IsAuthenticated,IsParent | IsTeacher | IsStudent)
    def get_object(self, request):
        try:
            return Notice.objects.filter(date_created__range = [request.data['from_date'],request.data['to_date']])
        except Notice.DoesNotExist:
            return Response(status =HTTP_404_NOT_FOUND)
    def post(self, request, format = None):
        notice = self.get_object(request)
        serializer = GetNoticeSerializer(notice, many = True)
        return Response(serializer.data, status = HTTP_200_OK)



class EventView(APIView):
    
    def get_permissions(self):
        if self.request.method == 'GET':
            permission_classes = (IsAuthenticated,IsParent | IsStudent | IsTeacher,)
        else:
            permission_classes = (IsAuthenticated,IsPrincipal)
        return [permission() for permission in permission_classes]

    def post(self, request, format = None):
        data = {}
        for i in range(len(request.data)):
            classes_id = ClassesSection.objects.filter(Q(classes = request.data[i]['classes'])&
            Q(section = request.data[i]['section'])).values('id')
            serializer = EventSerializer(data = {"title": request.data[i]['title'],"classes":classes_id[0]['id'], "description":request.data[i]['description']})
            if serializer.is_valid():
                serializer.save(created_by = request.user)
                data['response'] = 'Event Created'
            else:
                data = serializer.errors
        return Response(data)

    def get_specific_object(self, request):
        try:
            return Event.objects.filter(
               Q(classes__student__registration_number = request.user) |
               Q(classes__student__parent__father_contact_number = request.user)
            )
        except Event.DoesNotExist:
            return Response(status = HTTP_404_NOT_FOUND)
    def get_object(self, request):
        try:
            return Event.objects.all()
        except Event.DoesNotExist:
            return Response(status = HTTP_404_NOT_FOUND)
    def get(self, request, format = None):
        required_role = request.user.roles.all()
        if required_role.filter(Q(role='Student') | Q(role = 'Parent')).exists():
            event = self.get_specific_object(request)
        else:
            event = self.get_object(request)
        serializer = GetEventSerializer(event, many = True)
        return Response(serializer.data)

class EventViewDateFilter(APIView):
    permission_classes = (IsAuthenticated,IsParent | IsTeacher | IsStudent)
    def get_object(self, request):
        try:
            return Event.objects.filter(date_created__range = [request.data['from_date'],request.data['to_date']])
        except Event.DoesNotExist:
            return Response(status =HTTP_404_NOT_FOUND)
    def post(self, request, format = None):
        event = self.get_object(request)
        serializer = GetEventSerializer(event, many = True)
        return Response(serializer.data, status = HTTP_200_OK)

class HolidayView(APIView):
    def get_permissions(self):
        if self.request.method == 'GET':
            permission_classes = (IsAuthenticated,IsParent | IsStudent | IsTeacher,)
        else:
            permission_classes = (IsAuthenticated,IsPrincipal)
        return [permission() for permission in permission_classes]

    def post(self, request, format = None):
        data = {}
        for i in range(len(request.data)):
            classes_id = ClassesSection.objects.filter(Q(classes = request.data[i]['classes'])&
            Q(section = request.data[i]['section'])).values('id')
            serializer = HolidaySerializer(data = {"title": request.data[i]['title'],"classes":classes_id[0]['id'],"description":request.data[i]['description']})
            if serializer.is_valid():
                serializer.save(created_by = request.user)
                data['response'] = 'Holiday Created'
            else:
                data = serializer.errors
        return Response(data)

    def get_specific_object(self, request):
        try:
            return Holiday.objects.filter(
            Q(classes__student__registration_number = request.user) |
            Q(classes__student__parent__father_contact_number = request.user)
            )
        except Holiday.DoesNotExist:
            return Response(status = HTTP_404_NOT_FOUND)
    def get_object(self, request):
        try:
            return Holiday.objects.all()
        except Holiday.DoesNotExist:
            return Response(status = HTTP_404_NOT_FOUND)
    def get(self, request, format = None):
        required_role = request.user.roles.all()
        if required_role.filter(Q(role='Student') | Q(role = 'Parent')).exists():
            holiday = self.get_specific_object(request)
        else:
            holiday = self.get_object(request)
        serializer = GetHolidaySerializer(holiday, many = True)
        return Response(serializer.data)

class HolidayViewDateFilter(APIView):
    permission_classes = (IsAuthenticated,IsParent | IsTeacher | IsStudent)
    def get_object(self, request):
        try:
            return Holiday.objects.filter(date_created__range = [request.data['from_date'],request.data['to_date']])
        except Holiday.DoesNotExist:
            return Response(status =HTTP_404_NOT_FOUND)
    def post(self, request, format = None):
        holiday = self.get_object(request)
        serializer = GetHolidaySerializer(holiday, many = True)
        return Response(serializer.data, status = HTTP_200_OK)