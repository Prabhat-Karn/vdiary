from rest_framework.views import APIView
from rest_framework.response import Response
from attendance.api.serializers import TeacherAttendanceSerializer, StudentAttendanceSerializer, GetTeacherAttendanceSerializer, GetStudentAttendanceSerializer
from Vdiary.permissions import IsTeacher, IsParent, IsStudent, IsPrincipal, IsClassTeacher
from rest_framework.permissions import IsAuthenticated
# from rest_framework import generics
from rest_framework.status import(
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)

from attendance.models import TeacherAttendance, StudentAttendance

from subject.models import Subject
from lecture.models import Lecture
from classes.models import ClassesSection
from profiles.models import Teacher, Student
from account.models import Account

from django.db.models import Q
from datetime import date
import datetime

from rest_framework.authentication import SessionAuthentication, TokenAuthentication
import json
class TotalLectureView(APIView):
    permission_classes = (IsAuthenticated,IsTeacher,)
    def post(self, request, format = None):
        start_date = date.fromisoformat(request.data['from_date'])
        end_date = date.fromisoformat(request.data['to_date'])
        delta = datetime.timedelta(days = 1)
        total_lecture_details = []
        while start_date <= end_date:
            required_role = request.user.roles.all()
            if required_role.filter(Q(role='Principal')).exists() and 'teacher_id' in request.data:
                attendance = TeacherAttendance.objects.filter(Q(attendance = True)
                &Q(attendance_date=start_date)
                &Q(teacher__teacher_id = request.data['teacher_id'])).values('attendance_date')
                total_class_count = attendance.count()
            else:
                attendance = TeacherAttendance.objects.filter(Q(attendance = True)
                &Q(attendance_date=start_date)
                &Q(teacher__contact_number = request.user)).values('attendance_date')
                total_class_count = attendance.count()

            if attendance:
                total_lecture_details.append({'date':attendance[0]['attendance_date'],'total_class':total_class_count})
            else:
                pass
            start_date += delta

        return Response(total_lecture_details, status = HTTP_200_OK)

class TotalLectureTillDate(APIView):
    permission_classes = (IsAuthenticated,IsPrincipal,)

    def get_total_lecture(self, request):
        try:
            return TeacherAttendance.objects.filter(Q(attendance = True)
                &Q(teacher__teacher_id = request.data['teacher_id'])).count()
        except TeacherAttendanceDoesNotExist:
            return Response(status = HTTP_404_NOT_FOUND)
    def post(self, request, format = None):
        total_attendance = self.get_total_lecture(request)
        return Response({'total_lecture':total_attendance})

class CheckTeacherAttendance(APIView):
    permission_classes = (IsAuthenticated,IsTeacher,)
    def get_object_for_principal(self, request):
        try:
            return TeacherAttendance.objects.filter(Q(teacher__teacher_id = request.data['teacher_id'])&
            Q(attendance_date = request.data['date'])&
            Q(attendance = True))
        except TeacherAttendance.DoesNotExist:
            return Response(status = HTTP_404_NOT_FOUND)

    def get_object(self, request):
        try:
            return TeacherAttendance.objects.filter(Q(teacher__contact_number = request.user)&
            Q(attendance_date = request.data['date'])&
            Q(attendance = True))
        except TeacherAttendance.DoesNotExist:
            return Response(status = HTTP_404_NOT_FOUND
            )
    def post(self, request, format = None):
        required_role = request.user.roles.all()
        if required_role.filter(Q(role='Principal')).exists() and 'teacher_id' in request.data:
            attendance = self.get_object_for_principal(request)
        else:
            attendance = self.get_object(request)
        serializer = GetTeacherAttendanceSerializer(attendance, many = True)
        return Response(serializer.data)

class TeacherAttendanceView(APIView):
    serializer_class = TeacherAttendanceSerializer, GetTeacherAttendanceSerializer
    permission_classes = (IsAuthenticated,IsTeacher,)

    # def get_object(self, request):
    #     try:
    #         return TeacherAttendance.objects.filter(teacher__contact_number = request.user)
    #     except TeacherAttendance.DoesNotExist:
    #         return Response(status=status.HTTP_404_NOT_FOUND)

    # def get(self, request, format = None):
    #     attendance = self.get_object(request)
    #     serializer = GetTeacherAttendanceSerializer(attendance, many = True)
    #     return Response(serializer.data)

    def post(self, request, format = None):
        serializer = TeacherAttendanceSerializer(data = request.data)

        sub_id = Subject.objects.filter(Q(subject_name = request.data['subject'])&
        Q(classes__classes = request.data['classes'])& Q(classes__section = request.data['section'])).values('id')

        lec_id = Lecture.objects.filter(Q(lecture_no = request.data['lecture'])&
        Q(classes__classes = request.data['classes'])& Q(classes__section = request.data['section'])).values('id')

        classes_id = ClassesSection.objects.filter(Q(classes = request.data['classes'])&
        Q(section = request.data['section'])).values('id')

        teacher_id = Teacher.objects.filter(Q(contact_number = request.user))
        print(teacher_id)
        print(request.user)
        data = {}
        if serializer.is_valid():
            teacher_attendance = serializer.save(teacher_id = teacher_id[0],attendance=True, 
                                    subject_id = sub_id[0]['id'], lecture_id = lec_id[0]['id'],classes_id = classes_id[0]['id'])
            data['response'] = 'Attendance Doneee!'
            
            students = list(Student.objects.filter(Q(classes = teacher_attendance.classes)).values('registration_number__registration_number','roll_no',
                                'first_name','middle_name','last_name'))


            data['students'] = students

        else:
            data = serializer.errors
        return Response(data)

class StudentAttendanceView(APIView):
    serializer_class = StudentAttendanceSerializer
    authentication_classes = (SessionAuthentication,TokenAuthentication)

    def get_permissions(self):
        if self.request.method == 'GET':
            permission_classes = (IsAuthenticated,IsParent | IsTeacher)
        else:
            permission_classes = (IsAuthenticated,IsTeacher,)
        return [permission() for permission in permission_classes]
    
    def get_attendance(self,pk):
        return StudentAttendance.objects.get(pk=pk)

    def post(self, request, format = None):
        
        students = json.loads(request.body)
        classes_id = ClassesSection.objects.filter(Q(classes = students[0]['classes'])& Q(section = students[0]['section'])).values('id')
        sub_id = Subject.objects.filter(Q(subject_name = students[0]['subject'])&
        Q(classes__classes = students[0]['classes'])& Q(classes__section = students[0]['section'])).values('id')

        lec_id = Lecture.objects.filter(Q(lecture_no = students[0]['lecture'])).values('id')
        for i in students:
            account_id = Account.objects.filter(Q(registration_number = i['student'])).values('id')
            student_id = Student.objects.filter(Q(registration_number__id=account_id[0]['id'])).values('id')
            attendance = i['attendance']

            serializer = StudentAttendanceSerializer(data = {"student":student_id[0]['id'],"classes":classes_id[0]['id'],'lecture':lec_id[0]['id'],
                                            "subject":sub_id[0]['id'], 'attendance':attendance
                                        })
            data = {}
            if serializer.is_valid():
                attendance = serializer.save()
                data['response'] = 'Attendance Done!'

            else:
                data = serializer.errors

        return Response(data)

    def patch(self, request, pk, format = None):
        attendance = self.get_attendance(pk)
        serializer = StudentAttendanceSerializer(attendance,data = request.data, partial = True)
        data = {}
        if serializer.is_valid():
            attendance = serializer.save()
            data['response'] = 'Attendance Updated!'

        else:
            data = serializer.errors

        return Response(data)

class CheckStudentAttendance(APIView):
    permission_classes = (IsAuthenticated,IsParent | IsTeacher)

    # def get(self, request, registration_number = 'registration_number', subject = 'subject'):
    #     att_summary = []
    #     student_data = StudentAttendance.objects.select_related('student').filter(student__registration_number__registration_number = registration_number,subject__subject_name=subject)
    #     for i in student_data:
    #         att_summary.append({'roll_no':i.student.roll_no,'first_name':i.student.first_name,
    #                             'middle_name':i.student.middle_name, 'last_name':i.student.last_name,
    #                             'attendance':i.attendance, 'attendance_date':i.attendance_date})
    #     return Response(att_summary)

    def post(self, request, format = None):
        attendance_summary = []

        total_class = TeacherAttendance.objects.filter(Q(attendance=True)&
        # Q(attendance_date__range=['2020-03-1','2021-03-1'])&
        Q(classes__classes = request.data['classes'])&
        Q(classes__section = request.data['section'])&
        # Q(lecture__lecture_no = request.data['lecture'])&
        Q(subject__subject_name = request.data['subject'])).count()

        student_details = StudentAttendance.objects.select_related('student').filter(
            Q(classes__classes = request.data['classes'])&
            Q(classes__section = request.data['section'])&
            Q(subject__subject_name = request.data['subject'])
            # Q(lecture__lecture_no = request.data['lecture'])
        ).values('student','student__roll_no','student__registration_number__registration_number','student__first_name','student__middle_name','student__last_name').distinct()
        for i in student_details:
            class_attended = StudentAttendance.objects.filter(Q(attendance = True)&
            # Q(attendance_date__range=['2020-03-1','2021-03-1'])&
            Q(subject__subject_name = request.data['subject'])&
            # Q(lecture__lecture_no = request.data['lecture'])&
            Q(student = i['student'])).count()

            absent = StudentAttendance.objects.filter(Q(attendance = False)&
            # Q(attendance_date__range = ['2020-03-1','2021-03-1'])&
            Q(subject__subject_name = request.data['subject'])&
            # Q(lecture__lecture_no = request.data['lecture'])&
            Q(student = i['student'])).count()

            percentage = class_attended/total_class * 100
            
            attendance_summary.append({'student_roll_no':i['student__roll_no'],'student_name':i['student__first_name']+' '+str(i['student__middle_name'])+' '+str(i['student__last_name']),'registration_number':i['student__registration_number__registration_number'],'total_class':total_class,
            'class_attended':class_attended,'absent':absent, 'percentage':'{:.2f}'.format(percentage)+'%'})
        
        return Response(attendance_summary, status = HTTP_200_OK)

class CheckDetailStudentAttendance(APIView):
    permission_classes = (IsAuthenticated,IsParent | IsTeacher | IsStudent)
    def post(self, request, format = None):
        attendance_details = []
        student_details = StudentAttendance.objects.select_related('student').filter(
            Q(attendance_date__range=[request.data['from_date'],request.data['to_date']])&
            Q(student__registration_number__registration_number = request.data['registration_number'])&
            Q(subject__subject_name = request.data['subject'])
        ).values('student','student__roll_no','student__registration_number__registration_number','student__first_name','student__middle_name','student__last_name','attendance','attendance_date').distinct()
        
        for i in student_details:
            attendance_details.append({'student_roll_no':i['student__roll_no'],'name':i['student__first_name']+' '+str(i['student__middle_name'])+' '+str(i['student__last_name']),'attendance':i['attendance'],'attendance_date':i['attendance_date']})
        
        return Response(attendance_details, status = HTTP_200_OK)

class UpdateStudentAttendance(APIView):
    permission_classes = (IsAuthenticated, IsTeacher)
    # def get_attendance(self,pk):
    #     return StudentAttendance.objects.get(pk=pk)

    def post(self, request, format = None):
        student_attendance_details = []
        student_details = StudentAttendance.objects.select_related('student').filter(
            Q(attendance_date=request.data['date'])&
            Q(classes__classes = request.data['classes'])&
            Q(classes__section = request.data['section'])&
            Q(subject__subject_name = request.data['subject'])
        ).values('id','student__roll_no','student__registration_number__registration_number','student__first_name','student__middle_name','student__last_name','attendance').distinct()
        
        for i in student_details:
            student_attendance_details.append({'id':i['id'],'registration_number':i['student__registration_number__registration_number'],'student_roll_no':i['student__roll_no'],'name':i['student__first_name']+' '+str(i['student__middle_name'])+' '+str(i['student__last_name']),'attendance':i['attendance']})
        
        return Response(student_attendance_details, status = HTTP_200_OK)

    def validate_ids(self, id_list):
            for id in id_list:
                try:
                    StudentAttendance.objects.get(id=id)
                except (StudentAtttendance.DoesNotExist, ValidationError):
                    raise status.HTTP_400_BAD_REQUEST
            return True
    def get_obj(self, obj_id):
        try:
            return StudentAttendance.objects.get(id = obj_id)
        except (StudentAtttendance.DoesNotExist, ValidationError):
            raise status.HTTP_400_BAD_REQUEST
    def put(self, request, format = None):
        data = request.data
        attendance_ids = [i['id'] for i in data]
        self.validate_ids(attendance_ids)
        # instances = []
        for att_dict in data:
            attendance_id = att_dict['id']
            # registration_number = att_dict['registration_number']
            attendance = att_dict['attendance']
            obj = self.get_obj(attendance_id)
            # obj.student__registration_number__registration_numbers = registration_number
            obj.attendance = attendance
            obj.save()
        return Response({'Response':'Attendance Updated'}, status = HTTP_200_OK)

class CheckStudentAttendanceEverySub(APIView):
    permission_classes = (IsAuthenticated,IsPrincipal | IsClassTeacher | IsParent | IsStudent)

    def post(self, request, format = None):
        attendance_summary = []
        subjects = Subject.objects.filter(Q(classes__student__registration_number__registration_number = request.data['registration_number'])).values('id','subject_name')
        # print(subjects[0])
        for i in subjects:
            print(i)
            total_class = TeacherAttendance.objects.filter(Q(attendance=True)&
            Q(subject = i['id'])).count()

            class_attended = StudentAttendance.objects.filter(Q(attendance = True)&
            Q(subject = i['id']) & Q(student__registration_number__registration_number = request.data['registration_number'])).count()

            absent = StudentAttendance.objects.filter(Q(attendance = False)&
            Q(subject = i['id']) & Q(student__registration_number__registration_number = request.data['registration_number'])).count()
            reg_no = request.data['registration_number']
            if total_class != 0:
                percentage =  class_attended/total_class * 100
                attendance_summary.append({'registration_number':reg_no,'subject':i['subject_name'], 'total_class':total_class,
                'class_attended':class_attended,'absent':absent,'percentage':'{:.2f}'.format(percentage)+'%'})
            else:
                continue
        return Response(attendance_summary, status = HTTP_200_OK)

class CheckStudentAttendanceHome(APIView): #check attendance by parent and student in their home screen
    permission_classes = (IsAuthenticated, IsParent | IsStudent)

    def get_student_attendance(self, request):
        student_attendance = StudentAttendance.objects.filter(Q(student__registration_number = request.user) &
                                                            Q(attendance_date__range = [request.data['from_date'],request.data['to_date']]))
        return student_attendance

    def get_student_attendance_for_parent(self, request):
        student_attendance = StudentAttendance.objects.filter(Q(student__registration_number__registration_number = request.data['registration_number'])&
                                                            Q(attendance_date__range = [request.data['from_date'],request.data['to_date']]))
        return student_attendance

    def post(self, request, format = None):
        required_role = request.user.roles.all()
        if required_role.filter(Q(role = 'Parent')).exists():
            student_attendance = self.get_student_attendance_for_parent(request)
        else:
            student_attendance = self.get_student_attendance(request)
        serializer = GetStudentAttendanceSerializer(student_attendance, many = True)
        return Response(serializer.data, status = HTTP_200_OK)
