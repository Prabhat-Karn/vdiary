from rest_framework import serializers
from attendance.models import TeacherAttendance, StudentAttendance
class GetTeacherAttendanceSerializer(serializers.ModelSerializer):
    subject = serializers.CharField(source = 'subject.subject_name', read_only = True)
    lecture = serializers.CharField(source = 'lecture.lecture_no', read_only = True)
    classes = serializers.CharField(source = 'classes.classes', read_only = True)
    section = serializers.CharField(source = 'classes.section', read_only = True)
    class Meta:
        model = TeacherAttendance
        fields = ['classes','section', 'lecture', 'subject','attendance', 'attendance_date','attendance_time']
class TeacherAttendanceSerializer(serializers.ModelSerializer):
    subject = serializers.CharField(source = 'subject.subject_name', read_only = True)
    lecture = serializers.CharField(source = 'lecture.lecture_no', read_only = True)
    classes = serializers.CharField(source = 'classes.classes', read_only = True)
    section = serializers.CharField(source = 'classes.section', read_only = True)
    class Meta:
        model = TeacherAttendance
        fields = ['classes','section', 'lecture', 'subject', 'attendance_date','attendance_time']

class StudentAttendanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentAttendance
        fields = ['student','classes', 'lecture', 'subject','attendance', 'attendance_date','attendance_time']

class GetStudentAttendanceSerializer(serializers.ModelSerializer):
    subject = serializers.CharField(source = 'subject.subject_name', read_only = True)
    lecture = serializers.CharField(source = 'lecture.lecture_no', read_only = True)
    classes = serializers.CharField(source = 'classes.classes', read_only = True)
    section = serializers.CharField(source = 'classes.section', read_only = True)
    class Meta:
        model = StudentAttendance
        fields = ['classes','section','lecture','subject','attendance','attendance_date','attendance_time']