from django.urls import path
from attendance.api import views

app_name = 'attendance'


urlpatterns = [
    path('teacher_attendance/', views.TeacherAttendanceView.as_view(), name = 'get teacher attendance'),
    path('student_attendance/',views.StudentAttendanceView.as_view(), name = 'student attendance'),
    path('check_attendance/',views.CheckStudentAttendance.as_view(),name = 'check attendance'),
    path('check_detail_attendance/',views.CheckDetailStudentAttendance.as_view(),name = 'check attendance'),
    path('get_attendance_detail/',views.UpdateStudentAttendance.as_view(),name = 'get attendance'),
    # path('check_attendance/<registration_number>/<subject>',views.CheckStudentAttendance.as_view(),name = 'count'),
    path('update_student_attendance/',views.UpdateStudentAttendance.as_view(),name = 'update attendance'),
    path('check_teacher_attendance/',views.CheckTeacherAttendance.as_view(),name = 'check teacher attendance'),
    path('total_lec/',views.TotalLectureView.as_view(),name = 'total lecture'),
    path('check_student_attendance_sub/',views.CheckStudentAttendanceEverySub.as_view(),name = 'check student attendance'), #student attendance of every subject for classteacher
    path('student_attendance_home/',views.CheckStudentAttendanceHome.as_view(),name = 'check student attendance'), #check attendance by parent and student in their home screen
    path('total_lecture_till_date/',views.TotalLectureTillDate.as_view(),name = 'Total lecture count till date')
]