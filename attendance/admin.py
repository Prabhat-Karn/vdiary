from django.contrib import admin
from attendance.models import TeacherAttendance, StudentAttendance
# Register your models here.

class TeacherAttendanceAdmin(admin.ModelAdmin):
    list_display = ('id','teacher','classes','lecture','attendance','attendance_date', 'subject')
    search_fields = ('id',)

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

admin.site.register(TeacherAttendance, TeacherAttendanceAdmin)

class StudentAttendanceAdmin(admin.ModelAdmin):
    list_display = ('id','student','classes','lecture','attendance','attendance_date', 'subject')
    search_fields = ('id',)

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

admin.site.register(StudentAttendance, StudentAttendanceAdmin)
