from django.db import models

from classes.models import ClassesSection
from profiles.models import Student, Teacher
from lecture.models import Lecture
from subject.models import Subject
# Create your models here.

class TeacherAttendance(models.Model):
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    classes = models.ForeignKey(ClassesSection, on_delete=models.CASCADE)
    # section = models.ForeignKey(ClassesSection, on_delete=models.CASCADE)
    lecture = models.ForeignKey(Lecture, on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    attendance = models.BooleanField()
    attendance_date = models.DateField(auto_now_add=True)
    attendance_time = models.TimeField(auto_now_add=True)

class StudentAttendance(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    classes = models.ForeignKey(ClassesSection, on_delete=models.CASCADE)
    # section = models.ForeignKey(Section, on_delete=models.CASCADE)
    lecture = models.ForeignKey(Lecture, on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    attendance = models.BooleanField()
    attendance_date = models.DateField(auto_now_add=True)
    attendance_time = models.TimeField(auto_now_add=True)