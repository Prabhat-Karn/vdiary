from django.contrib import admin
from subject.models import Subject
from import_export.admin import ImportExportModelAdmin
from import_export import fields, resources
from import_export.widgets import ForeignKeyWidget
from classes.models import ClassesSection
# Register your models here.

class SubjectResource(resources.ModelResource):
    id = fields.Field(
        column_name = 'id',
        attribute = 'id'
    )
    subject_name = fields.Field(
        column_name = 'subject_name',
        attribute = 'subject_name'
    )
    
    classes = fields.Field(
        column_name = 'classes',
        attribute = 'classes',
        widget = ForeignKeyWidget(ClassesSection, 'id')
    )
    date_created = fields.Field(
        column_name = 'date_created',
        attribute = 'date_created'
    )

    class Meta:
        model = Subject
        fields = ('id','subject_name','classes','date_created')

class SubjectAdmin(ImportExportModelAdmin,admin.ModelAdmin):
    resource_class = SubjectResource
    list_display = ('id','subject_name','classes','date_created')
    search_fields = ('id','subject_name')
    readonly_fields = ('date_created',)

admin.site.register(Subject, SubjectAdmin)