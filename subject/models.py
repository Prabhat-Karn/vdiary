from django.db import models
from profiles.models import Teacher
from classes.models import ClassesSection
# Create your models here.

class Subject(models.Model):
    subject_name = models.CharField(max_length=50)
    classes = models.ForeignKey(ClassesSection, on_delete=models.CASCADE)
    # section = models.ForeignKey(ClassesSection, on_delete=models.CASCADE)
    date_created = models.DateTimeField(verbose_name = "date created", auto_now_add=True)

    class Meta:
        unique_together = ('subject_name','classes')

    def __str__(self):
        return self.subject_name