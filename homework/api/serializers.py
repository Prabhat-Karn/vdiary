from rest_framework import serializers
from homework.models import AssignHomework

class AssignHomeworkSerializer(serializers.ModelSerializer):
    class Meta:
        model = AssignHomework
        fields = ['title','description','homework_file','date_created']