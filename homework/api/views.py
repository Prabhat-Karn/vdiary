from rest_framework.views import APIView
from rest_framework.response import Response
from Vdiary.permissions import IsParent, IsTeacher, IsStudent
from rest_framework.permissions import IsAuthenticated

from homework.models import AssignHomework
# from classwork.models import AssignClasswork
# from remarks.models import Remarks
from homework.api.serializers import AssignHomeworkSerializer
# from classwork.api.serializers import AssignClassworkSerializer
from profiles.models import Student

from django.db.models import Q

from attendance.models import TeacherAttendance
from django.shortcuts import render, redirect
from django.views import View


class AssignHomeworkView(APIView):
    serializer_class = AssignHomeworkSerializer
    
    def get_permissions(self):
        if self.request.method == 'GET':
            permission_classes = (IsAuthenticated,IsParent | IsStudent,)
        else:
            permission_classes = (IsAuthenticated,IsTeacher,)
        return [permission() for permission in permission_classes]

    def get_object(self, request):
        try:
            return AssignHomework.objects.filter(Q(classes__student__parent__father_contact_number = request.user) |
            Q(classes__student__registration_number = request.user)).distinct()
        except AssignHomework.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def get_homework(self, pk):
        return AssignHomework.objects.get(pk=pk)

    def get(self, request, pk=None, format = None):
        homework = self.get_object(request)
        serializer = AssignHomeworkSerializer(homework, many = True)
        return Response(serializer.data)

    def post(self, request, pk=None, format = None):
        attendance_data =TeacherAttendance.objects.filter(teacher__contact_number = request.user)
        teacher = list(attendance_data)[-1].teacher_id
        classes = list(attendance_data)[-1].classes_id
        # section = list(attendance_data)[-1].section_id
        lecture = list(attendance_data)[-1].lecture_id
        subject = list(attendance_data)[-1].subject_id

        serializer = AssignHomeworkSerializer(data = request.data)
        data = {}
        if serializer.is_valid():
            classwork = serializer.save(teacher_id = teacher, classes_id = classes, 
                        lecture_id = lecture, subject_id = subject)
            data['response'] = 'Homework Assigned'

        else:
            data = serializer.errors

        return Response(data)

    def patch(self, request, pk, format = None):
        homework = self.get_homework(pk)
        serializer = AssignHomeworkSerializer(homework,data = request.data, partial = True)
        data = {}
        if serializer.is_valid():
            homework = serializer.save()
            data['response'] = 'Homework Updated!'

        else:
            data = serializer.errors

        return Response(data)