from django.urls import path
from homework.api import views

app_name = 'homework'


urlpatterns = [
    path('homework/', views.AssignHomeworkView.as_view(), name = 'homework'),
]