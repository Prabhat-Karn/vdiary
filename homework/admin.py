from django.contrib import admin
from homework.models import AssignHomework
# Register your models here.
class AssignHomeworkAdmin(admin.ModelAdmin):
    list_display = ('id','teacher','classes','lecture','subject','title','description','date_created')
    search_fields = ('id','title')

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

admin.site.register(AssignHomework, AssignHomeworkAdmin)