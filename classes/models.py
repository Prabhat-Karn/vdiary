from django.db import models

# Create your models here.
class Section(models.Model):
    section_name = models.CharField(max_length=5, primary_key = True)
    date_created = models.DateTimeField(verbose_name = "date created", auto_now_add=True)
    
    def __str__(self):
        return self.section_name

class Classes(models.Model):
    # class_name = models.CharField(max_length=10, primary_key = True)
    class_name = models.PositiveIntegerField(primary_key = True)
    section_name = models.ManyToManyField(Section, through = 'ClassesSection')
    date_created = models.DateTimeField(verbose_name = "date created", auto_now_add=True)

    def __str__(self):
        return str(self.class_name)

class ClassesSection(models.Model):
    classes = models.ForeignKey(Classes, on_delete=models.CASCADE)
    section = models.ForeignKey(Section, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.classes) +' '+ str(self.section)
    class Meta:
        unique_together = ('section','classes')
