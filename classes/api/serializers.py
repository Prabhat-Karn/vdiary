from rest_framework import serializers
from classes.models import ClassesSection

class ClassesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClassesSection
        fields = ['classes','section']