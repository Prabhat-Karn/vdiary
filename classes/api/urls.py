from django.urls import path
from classes.api import views

app_name = 'classes'


urlpatterns = [
    path('classes/', views.ClassesView.as_view(), name = 'class'),
]