from rest_framework.views import APIView
from rest_framework.response import Response
from Vdiary.permissions import IsParent, IsTeacher, IsStudent
from rest_framework.permissions import IsAuthenticated

from classes.api.serializers import ClassesSerializer
from classes.models import ClassesSection

from rest_framework.authentication import SessionAuthentication, TokenAuthentication

class ClassesView(APIView):
    serializer_class = ClassesSerializer
    authentication_classes = (SessionAuthentication,TokenAuthentication)
    permission_classes = (IsAuthenticated,IsTeacher,)

    def get_object (self, request):
        try:
            return ClassesSection.objects.all().order_by('classes')
        except ClassesSection.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, format = None):
        classes = self.get_object(request)
        serializer = ClassesSerializer(classes, many = True)
        return Response(serializer.data)