from django.contrib import admin
from classes.models import Classes, Section, ClassesSection
# Register your models here.
class ClassesAdmin(admin.ModelAdmin):
    list_display = ('class_name','date_created')
    search_fields = ('class_name',)
    readonly_fields = ('date_created',)

    filter_horizontal = ()
    list_filter = ['class_name',]
    fieldsets = ()

class SectionAdmin(admin.ModelAdmin):
    list_display = ('section_name','date_created')
    search_fields = ('section_name_name',)
    readonly_fields = ('date_created',)

    filter_horizontal = ()
    list_filter = ['section_name',]
    fieldsets = ()

class ClassesSectionAdmin(admin.ModelAdmin):
    list_display = ('classes','section')

admin.site.register(Classes, ClassesAdmin)
admin.site.register(Section, SectionAdmin)
admin.site.register(ClassesSection, ClassesSectionAdmin)